# ppdp-kicad-library

A library of symbols and footprints used in projects by PPDP Electronics and other ventures.

## Licensing

The ppdp-kicad-library is licensed under a CC-BY-SA 4.0 license (with an exception) so please see the LICENSE.md for more information.
