# Components

- SN74CBT3245APWR https://www.digikey.com/en/products/detail/texas-instruments/SN74CBT3245APWR/377921
- PCA9535DBR https://www.digikey.com/en/products/detail/texas-instruments/PCA9535DBR/1216554
- ATTINY85-20SU https://www.digikey.com/en/products/detail/microchip-technology/ATTINY85-20SU/735470

# Lint

- Ensure all symbols have a datasheet
- Ensure all symbols have a digikey link or otherlink
- Ensure all symbols have a footprint
