#!/usr/bin/env node
const fs = require('fs');
const path = require('path');

let regex = /\s*\(tstamp \w{8}-\w{4}-\w{4}-\w{4}-\w{12}\)/g;
let newLineRegex = /(\r?\n){2,}/g;

const footprintDir = path.join(__dirname, '../../ppdp-footprints.pretty');
let footprints = fs.readdirSync(footprintDir);

footprints.forEach((filename) => {
  const fullFilename = path.join(footprintDir, filename);
  const contents = fs.readFileSync(fullFilename).toString();
  const newContents = contents.replaceAll(regex, '').concat('\n').replaceAll(newLineRegex, '\n');
  fs.writeFileSync(fullFilename, newContents);
});
