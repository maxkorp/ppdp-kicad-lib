#!/usr/bin/env node
const fs = require('fs');
const path = require('path');

let regex = /\s*\(tstamp \w{8}-\w{4}-\w{4}-\w{4}-\w{12}\)/;

const footprintDir = path.join(__dirname, '../../ppdp-footprints.pretty');
let footprints = fs.readdirSync(footprintDir);

let errors = [];
footprints.forEach((filename) => {
  const contents = fs.readFileSync(path.join(footprintDir, filename)).toString();
  if (regex.test(contents)) {
    errors.push(filename);
  }
});

if (errors.length) {
  console.warn(`The following files contain timestamps of the form "(tstamp xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx):"`);
  console.warn(errors.map((x) => '  ' + x).join('\n'));
  console.warn('Please remove these timestamps');
  process.exit(errors.length);
}
